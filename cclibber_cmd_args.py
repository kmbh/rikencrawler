import argparse


def _parse_arguments():
    parser = argparse.ArgumentParser(description="Parse log.xz files using cclib & other modules.")
    parser.add_argument("-d", "--directory", metavar='', nargs=1,
                        help="Specify a directory to look for .log.xz files and parse the GAMES data.")
    parser.add_argument("-f", "--file", type=file, metavar='', nargs=1,
                        help="Filepath(s) .log.xz file to parse.")
    parser.add_argument("-o", "--out_dir", nargs=1, metavar='', default=r"cclib_output_files",
                        help="Sets a new output directory name (not the path)." \
                             "The default dir name is [... work_dir\\]\"output_files\"")
    # parser.add_argument("-c", "--cid", type=int, action="append", nargs="*", metavar='',
    #                     help="CIDs written in cmd line will have output files retrieved.")
    # parser.add_argument("-p", "--preserve", action="store_true",
    #                     help="Preserve the current output files. "
    #                          "If not indicated, existing output files in working directory "
    #                          "will be deleted before running the program.")
    # parser.add_argument("-b", "--begin", type=int, nargs=1, metavar='',
    #                     help="All CIDs greater than or equal to this number will be downloaded.")
    # parser.add_argument("-s", "--source", nargs=1, metavar='', default=r"http://pubchemqc.riken.jp/",
    #                     help="Sets a new source address for the Riken website. " \
    #                          "The default address is \"http://pubchemqc.riken.jp/\"")

    return parser.parse_args()


CONSOLE_ARGS = _parse_arguments()