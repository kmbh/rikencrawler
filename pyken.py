from pyken_cmd_args import CONSOLE_ARGS
from itertools import chain
from requests import get, exceptions
from time import time, sleep
from os import path, makedirs, remove
from shutil import rmtree
import re


attempt_log = dict()
downloads = 0


def _read_file(filepath):
    with open(filepath, 'r') as f:
        content = f.readlines()
    return content


def _write_file(content_string, filepath):
    with open(filepath, "wb") as f:
        f.write(content_string)


def _if_downloaded(filepath):
    if path.isfile(filepath):
        return True
    return False


def _digest_cid_file(files=None):
    candidates, final = [], []
    if files:
        for f in files:
            with open(f, 'r') as x:
                content = x.read()
            candidates.extend(content.split(','))
            for candidate in candidates:
                try:
                    int(candidate)
                    final.append(int(candidate))
                except ValueError:
                    continue
    return final


def _write_temp_html(html_path, url):
    page = get(url)
    with open(html_path, 'wb') as t:
        t.write(page.content)


def handle_links(link_list, dir_path, parameters, start_time):
    for url in link_list:
        full_url = parameters.source + url
        new_file_path = path.join(dir_path, url.split('/')[2])
        if path.isfile(new_file_path) and not parameters.preserve:
            continue
        else:
            download_file(new_file_path, full_url, start_time, parameters.throttle)


def download_file(to_filepath, url, start_time, throttle=None):
    attempts = 0
    global downloads
    while True:
        try:
            if throttle and downloads > 0 and (60 * downloads)/(time() - start_time) > throttle:
                dur_to_sleep = (downloads - (throttle * (time() - start_time) / 60)) / throttle  # in Minutes
                print "\nSleeping for " + str(round(dur_to_sleep * 60, 1)) + "s\n"
                sleep(dur_to_sleep * 60)  # Must convert to seconds for sleep function
            downloaded_file = get(url)
            with open(to_filepath, 'wb') as out:
                out.write(downloaded_file.content)
            downloads += 1
            print to_filepath
            break
        except exceptions.RequestException as e:
            print e
            print "\n\'{0}\' failed to download.\n".format(url)
            attempt_log[url] = attempts
            attempts += 1
            if attempts < 4:
                sleep(5)
                continue
            else:
                break


def get_folder_structure(url):
    """This function returns the dir structure on Riken's website and the max/min cid value.
    The function relies on a "Compount_[9d cid]_[9d cid]" format for the dir names."""
    folder_info = []
    r = get(url)
    highest, lowest = 0, 0
    for i, line in enumerate(r.iter_lines()):
        # TODO: change these hard-coded digits
        if 61 <= i <= 2998:
            start = line.index(b"\">C")
            end = line.index(b"</a>")
            dir_name = line[start+1:end].decode("utf-8")[1:]
            dir_link = line[(line.find(b'href="') + 6):(line.find(b'">C'))].decode("utf-8")
            folder_info.append((dir_name, dir_link))
            limits = [int(x) for x in dir_name.replace('Compound_', '').split('_')]
            highest = max(highest, limits[0], limits[1])
            lowest = min(lowest, limits[0], limits[1])
    return folder_info, highest, lowest


def parse_folder_html(html, optional_cid=None):
    cid_dict = dict()
    regex = re.compile("(?:(?<!\d)\d{9}(?!\d))")
    p = re.compile('<.*?>')
    with open(html, 'r') as f:
        for i, line in enumerate(f):
            if '(generated)' in line:  # Filter out "generated" files
                continue
            if any(x in line for x in ['+']):
                if regex.search(line):
                    new_line = p.sub('', line)
                    cid = new_line[:new_line.find('.')].strip()
                    if optional_cid is not None and optional_cid != cid:
                        continue
                    if cid not in list(cid_dict.keys()):
                        cid_dict[cid] = []
                    url = line[line.find('<a href="') + 9:line.find('">')]
                    cid_dict[cid].append(url)
    return cid_dict


def main(param_args):
    start_time = time()
    working_dir = path.dirname(path.realpath(__file__))
    output_dir = path.join(working_dir, param_args.out_dir)
    temp_html = path.join(working_dir, "temp.html")
    if not param_args.preserve:
        rmtree(output_dir, ignore_errors=True)
        makedirs(output_dir)
    directories, high_cid, low_cid = get_folder_structure(param_args.source)
    cid_grab_bag = _digest_cid_file(param_args.file) + param_args.cid
    for name, url in directories:
        # try block here
        folder_url = param_args.source + url
        boundaries = [int(x) for x in name.replace('Compound_', '').split('_')]
        dir_file_path = path.join(output_dir, name)
        if param_args.begin and (param_args.begin in range(boundaries[0], boundaries[1] + 1)
                                 or param_args.begin <= boundaries[0]):
            _write_temp_html(temp_html, folder_url)
            download_links = parse_folder_html(temp_html, optional_cid=None)
            if not path.isdir(dir_file_path):
                makedirs(dir_file_path)
            for cid, links in download_links.items():
                if int(cid) >= param_args.begin:
                    print(cid)
                    handle_links(links, dir_file_path, param_args, start_time)
        elif cid_grab_bag:
            for cid in cid_grab_bag:
                if boundaries[0] <= cid <= boundaries[1]:
                    _write_temp_html(temp_html, folder_url)
                    # print str(cid).zfill(9)
                    download_links = parse_folder_html(temp_html, optional_cid=str(cid).zfill(9))
                    if str(cid).zfill(9) in download_links.keys():
                        if not path.isdir(dir_file_path):
                            makedirs(dir_file_path)
                        handle_links(download_links[str(cid).zfill(9)], dir_file_path, param_args, start_time)
            continue
        else:
            if param_args.begin:
                continue
            if not path.isdir(dir_file_path):
                makedirs(dir_file_path)
            _write_temp_html(temp_html, folder_url)
            download_links = parse_folder_html(temp_html, optional_cid=None)
            for cid, links in download_links.items():
                handle_links(links, dir_file_path, param_args, start_time)
    if path.isfile(temp_html):
        remove(temp_html)


if __name__ == '__main__':
    params = CONSOLE_ARGS
    if params.file:
        params.file = list(chain(*params.file))
    else:
        params.file = []
    if params.cid:
        params.cid = list(chain(*params.cid))
    else:
        params.cid = []
    print params
    main(params)
