
import datetime as dt
import requests
import time
import os
from sys import platform
import lzma
import openpyxl
import shutil
import re
from tqdm import tqdm

if platform == 'win32':
    clear_command = 'cls'
else:
    clear_command = 'clear'


def remove_html_tags(data):
  p = re.compile(r'<.*?>')
  return p.sub('', data)


def id_pubchem_num(data):
    regex = re.compile("(?:(?<!\d)\d{9}(?!\d))")
    if regex.search(data):
        return True
    else:
        return False


def parse_ids(filename_list):
    """Find all Pubchem Ids in each sdf file from list
     and return unique values as a sorted list of strings."""
    flag_str = '_modified_at_'
    pubchem_ids = []
    for filename in filename_list:
        with open(filename) as f:
            content = f.readlines()
        for i, line in enumerate(content):
            pubchem_id = None
            words = line.split()
            if flag_str in line:
                pos = line.find(flag_str)
                pubchem_id = line[:pos]
            elif len(words) == 1 and words[0].isdigit():
                pubchem_id = words[0]
            else:
                pubchem_id = None
            if pubchem_id is not None and pubchem_id not in pubchem_ids and len(pubchem_id) >= 7:
                pubchem_ids.append(pubchem_id)
    return sorted(pubchem_ids)


def parse_ids_2(filename):
    with open(filename, 'r') as f:
        content = f.readlines()
    return content


def get_match_info(id_list):
    """Get which pubchem ids are in pubchemqc"""
    matches = dict()
    total_found = 0
    start = time.time()
    # start_ts = dt.datetime.now()
    for i, pid in enumerate(id_list):
        #Riken: http://pubchemqc.riken.jp/cgi-bin/molecularquery.py?name=CIDxxxxx
        req_str = 'http://pubchemqc.riken.jp/cgi-bin/molecularquery.py?name=CID'
        empty_req_flag = 'Sorry, calculation is not yet'
        r = requests.get(req_str+str(pid))
        # print(r.text)
        if empty_req_flag not in r.text:
            url = get_next_href(r.text, 'GAMESS OUTPUT (ground):')
            if url != '':
                out_file = requests.get(url)
                create_file(out_file, pid, '.out', if_output=True)
                # create_file(out_file, pid, '.log', if_output=True)
                url = get_next_href(r.text, 'GAMESS INPUT (ground):')
                if url != '':
                    out_file = requests.get(url)
                    create_file(out_file, pid, '.inp', if_output=False)
                matches[pid] = r'D:/GitApps/Swamidass/inp_files/' + pid + '.out'
                status = ' $$FOUND$$'
            else:
                matches[pid] = r'CHECK'
                status = ' $$FOUND-CHECK$$'
            total_found += 1
        else:
            matches[pid] = 'NOT FOUND'
            status = ' not found'
        elapsed_time = time.time() - start
        avg_job_time = elapsed_time / (i+1)
        est_end = dt.datetime.now() + dt.timedelta(seconds=(avg_job_time*( len(id_list)-(i+1) )))
        perc_found = round(100 * total_found/len(id_list), 1)
        perc_finished = round(100 * (i + 1)/len(id_list), 1)
        # os.system(clear_command)
        print("\n\nID " + str(i + 1) + " CID" + pid + status)
        print(str(i + 1) + " / " + str(len(id_list)) + ", " + str(perc_finished) + "% COMPLETED")
        print("Estimated End Time: " + est_end.strftime("%I:%M:%S %p"))
        print(str(total_found) + " / " + str(len(id_list)) + ", " + str(perc_found)+"% FOUND")

    elapsed_time = time.time() - start
    os.system(clear_command)
    print('Finished. \n' + str(total_found) + ' ids identified in ' + str(round((elapsed_time/60), 1)) + "min.")
    return matches


def get_next_href(html, flag_str="GAMESS OUTPUT (ground):"):
    """Finds link to GAMESS input file on riken website, with HTML of page as input."""
    html = html.replace('  ', ' ')
    edited_html = html[html.find(flag_str)-1:]
    # print(edited_html)
    href_str = edited_html[edited_html.find('<a href="') + 9:edited_html.find('">')]
    # print(href_str, href_str.replace("..", "http://pubchemqc.riken.jp"))
    return href_str.replace("..", "http://pubchemqc.riken.jp")


def prep_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
    for the_file in os.listdir(path):
        file_path = os.path.join(path, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
            else:
                continue
        except Exception as e:
            print(e)


def create_file(content, name, ext, if_output=True):
    if if_output:
        with open(r'D:/GitApps/Swamidass/temp.xz', 'wb') as f:
            f.write(content.content)
        new_file_str = lzma.open('D:/GitApps/Swamidass/temp.xz').read()
    else:
        new_file_str = content.content
    with open(r'D:/GitApps/Swamidass/inp_files/' + name + ext, 'wb') as f:
        f.write(new_file_str)


def create_wb(matches, name_of_target=r'D:/GitApps/Swamidass/found_ids.xlsx'):
    created_filename = name_of_target
    wb = openpyxl.Workbook()
    w = wb.active
    w.cell(row=1, column=1).value = 'PID'
    w.cell(row=1, column=2).value = 'STATUS'
    for i, key in enumerate(matches.keys()):
        w.cell(row=2 + i, column=1).value = key
        w.cell(row=2 + i, column=2).value = matches[key]
    if os.path.exists(created_filename) and os.path.isfile(created_filename):
        os.unlink(created_filename)
    wb.save(created_filename)


def parse_by_html():
    source_addr = r"http://pubchemqc.riken.jp/"
    r = requests.get(source_addr)
    addresses = []
    matt_data = []
    pubchem_ids = dict()
    matches = dict()

    #Nothing in r'test_cids.txt'
    for f in [r'train_cids.txt']:
        content = parse_ids_2(f)
        for line in content:
            num = line.strip().zfill(9)
            # print(num)
            if line.strip() not in matt_data:
                matt_data.append(num)
                matches[num] = "No Match"
    # print(str(len(list(matches.keys()))))

    #get group addresses
    for i, line in enumerate(r.iter_lines()):
        if 61 <= i <= 2998:
            new_line = line[(line.find(b'href="')+6):(line.find(b'">C'))].decode("utf-8")
            addresses.append(source_addr+new_line)

    #get compounds and files available
    i = 0
    found_ids = 0
    for a in tqdm(addresses, ascii=True, desc="Address Parse"):
        if i > 3:
            break
        start = time.time()
        r = requests.get(a)
        print("\nReq took "+str(time.time()-start)+"s")
        start = time.time()
        for j, line in enumerate(r.iter_lines()):
            line_str = line.decode("utf-8")
            new_line = remove_html_tags(line_str)
            if id_pubchem_num(new_line):
                id = new_line[:new_line.find(".")].strip()
                ext = ("." + ".".join(map(str, [x for x in new_line.split(".") if "(d)" not in x if id not in x]))).replace("(generated)", "")
                if id not in list(pubchem_ids.keys()):
                    pubchem_ids[id] = []
                pubchem_ids[id].append(ext)
                if id in list(matches.keys()):
                    found_ids += 1
                    url = line_str[line_str.find('<a href="') + 9:line_str.find('">')]
                    if source_addr not in url:
                        url = source_addr + url
                    out_file = requests.get(url)
                    filename = new_line.replace("(generated)", "").strip()
                    with open(r'D:/GitApps/Swamidass/output_files/' + filename, 'wb') as x:
                        x.write(out_file.content)
                    matches[id] = "FOUND"
                else:
                    continue
                print("\nFound Id " + str(id))
                    # print(id, ext)
        print("Iteration took " + str(time.time()-start) + "s")
        i += 1
        print('\nFound: '+str(found_ids))

    create_wb(matches, name_of_target=r'D:/GitApps/Swamidass/riken_matches.xlsx')

    all_ids = dict()
    for k, v in list(pubchem_ids.items()):
        all_ids[k] = ";".join(map(str, v))
    create_wb(all_ids, name_of_target=r'D:/GitApps/Swamidass/riken_all.xlsx')


if __name__ == '__main__':
    # filename_list = ['test_set.sdf', 'training_set.sdf']
    # input_files_dir = r'D:\GitApps\Swamidass\inp_files'
    # prep_dir(input_files_dir)
    # ids = parse_ids(filename_list)
    # id_matches = get_match_info(ids)
    # create_wb(id_matches)
    # print('\nSuccess.')
    parse_by_html()

# import cclob
# myfile = cclib.io.ccopen(r'D:\GitApps\Swamidass\inp_files\5484476.out')
# data = myfile.parse()

# data.aonames
