"""

Command line parsing definition for Ryken Web Crawler.

"""

import argparse


def _parse_arguments():
    parser = argparse.ArgumentParser(description="Retrieve output files from the Riken website.")
    parser.add_argument("-t", "--throttle", type=float, metavar='',
                        help="Number of downloads per minute to set as limitation when running.")
    parser.add_argument("-f", "--file", type=file, action="append", nargs="*", metavar='',
                        help="Filepath(s) of csv(s) containing list of cids to retrieve.")
    parser.add_argument("-c", "--cid", type=int, action="append", nargs="*", metavar='',
                        help="CIDs written in cmd line will have output files retrieved.")
    parser.add_argument("-p", "--preserve", action="store_true",
                        help="Preserve the current output files. "
                             "If not indicated, existing output files in working directory "
                             "will be deleted before running the program.")
    parser.add_argument("-b", "--begin", type=int, nargs=1, metavar='',
                        help="All CIDs greater than or equal to this number will be downloaded.")
    parser.add_argument("-s", "--source", nargs=1, metavar='', default=r"http://pubchemqc.riken.jp/",
                        help="Sets a new source address for the Riken website. " \
                             "The default address is \"http://pubchemqc.riken.jp/\"")
    parser.add_argument("-o", "--out_dir", nargs=1, metavar='', default=r"output_files",
                        help="Sets a new output directory name (not the path)." \
                             "The default dir name is [... work_dir\\]\"output_files\"")
    return parser.parse_args()


CONSOLE_ARGS = _parse_arguments()
