
from cclibber_cmd_args import CONSOLE_ARGS
import cclib
import pandas
import numpy as np
import backports.lzma
import csv
import os


def unzip_file(xz_filepath):
    """
    This function unzips .xz files into content objects.
    :param xz_filepath: str of the xz file path
    :return: content object
    """
    with backports.lzma.open(xz_filepath) as f:
        content = f.read()
    return content


def write_file(content, filepath):
    """
    This function write a content object to a file at the specified file path.
    :param content: content object
    :param filepath: the target filepath for the file
    :return: None
    """
    with open(filepath, "wb") as f:
        f.write(content)


def get_games_data(filepath, return_parsed_obj=False):
    """
    This function parses a GAMES file (e.g. .txt file) for cclib data.
    :param filepath: Target file path of GAMES file
    :param return_parsed_obj: Whether to return the cclib "parsed object" or the attribute dict
    :return: Dictionary of all objects parsed from GAMES file by cclib
    """
    obj = cclib.parser.GAMESS(filepath)
    parsed_data = obj.parse()
    if return_parsed_obj:
        return parsed_data
    return parsed_data.getattributes()


def get_coulombmatrix(molecule, largest_mol_size=None):
    """
    This function generates a coulomb matrix for the given molecule
    if largest_mol size is provided matrix will have dimension lm x lm.
    Padding is provided for the bottom and right.
    Reference: https://stackoverflow.com/questions/47637578/how-to-create-coulomb-matrix-with-python
    :param molecule: dict object returned from cclib for molecule
    :param largest_mol_size: int of # atoms of the largest molecule
    :return: Coulomb Matrix
    """
    numberAtoms = molecule['natom']
    if largest_mol_size == None or largest_mol_size == 0:
        largest_mol_size = numberAtoms
    cij = np.zeros((largest_mol_size, largest_mol_size))

    xyzmatrix = molecule['atomcoords'][0]
    chargearray = molecule['atomnos']

    for i in range(numberAtoms):
        for j in range(numberAtoms):
            if i == j:
                cij[i][j] = 0.5 * chargearray[i] ** 2.4  # Diagonal term described by Potential energy of isolated atom
            else:
                dist = np.linalg.norm(np.array(xyzmatrix[i]) - np.array(xyzmatrix[j]))
                cij[i][j] = chargearray[i] * chargearray[j] / dist  # Pair-wise repulsion
    return cij


def get_fukuireactivity(molecule):
    """"""
    atomnos = molecule['atomnos']
    mullikens = molecule['atomcharges']['mulliken']



def get_mayerbo(games_filepath):
    d = cclib.method.MBO(get_games_data(games_filepath, return_parsed_obj=True))
    d.calculate()
    try:
        return d.fragresults
    except AttributeError as e:
        return e


def write_csv(array_obj, csv_targetpath):
    with open(csv_targetpath, 'wb') as csvfile:
        data_writer = csv.writer(csvfile, dialect="excel-tab")
        for line in array_obj:
            data_writer.writerow(line)


def get_xz_files(directory):
    xz_file_list = []
    for root, dirs, files in os.walk(directory):
        for f in files:
            if f.endswith(".log.xz"):
                xz_file_list.append(os.path.join(root, f))
    return xz_file_list


def cid_from_filename(filename):
    return filename.split(".")[0]


def get_tsv_data(games_file_path, cid, output_dir):
    # TODO: finish this function
    parsed_obj = get_games_data(games_file_path, return_parsed_obj=True)
    dict_obj = parsed_obj.getattributes()
    homo_ev = dict_obj['moenergies'][0][dict_obj['homos'][0]]
    lumo_ev = dict_obj['moenergies'][0][dict_obj['homos'][0] + 1]
    band_gap_ev = homo_ev - lumo_ev
    chemical_potential = (homo_ev + lumo_ev) / 2
    # write_csv(get_coulombmatrix(parsed_obj), os.path.join(output_dir, cid+"_coulomb.tsv"))  # Coulomb matrix

def main(parameters):
    files_to_parse = []
    working_dir = os.path.dirname(os.path.realpath(__file__))
    output_dir = os.path.join(working_dir, parameters.out_dir)
    temp = os.path.join(working_dir, "temp.txt")
    if parameters.directory:
        files_to_parse = get_xz_files(parameters.directory)
    if parameters.file:
        files_to_parse.append(parameters.file)
    if files_to_parse:
        for f in files_to_parse:
            cid = cid_from_filename(f)
            c = unzip_file(f)
            write_file(c, temp)
            get_tsv_data(temp, cid, output_dir)
    os.remove(temp)


if __name__ == "__main__":
    params = CONSOLE_ARGS
    main(params)


    # test_xz_path = r'/Users/Ernest/Desktop/test.log.xz'
    # test_games_path = r'/Users/Ernest/Desktop/out.txt'
    # csv_write_file = r'/Users/Ernest/Desktop/out.csv'
    # c = unzip_file(test_xz_path)
    # write_file(c, test_games_path)
    # data = get_games_data(test_games_path)
    # keys = sorted(data.keys())
    # print keys
    # print data['natom']
    # print len(data['moenergies'][0])
    # print data['atombasis']
    # print data['homos']
    # print data['moenergies'][0][data['homos'][0]]
    # print data['moenergies'][0]

    print data['scfenergies']

    # print len(data['moenergies'][0])/data['atomnos']
    # cclib.method.Orbitals()
    # write_csv([data['atomcharges']['mulliken']], csv_write_file)

    # Coulomb Matrix --> get_coulombmatrix(data)
    # Mulliken Charges --> data['atomcharges']['mulliken']
    # Fukui Reactivity -->
    # Mayer Bond Order --> get_mayerbo()
    # Atomization Energy
    # Band Gap HOMO & LUMO
    # HOMO eV = data['moenergies'][0][data['homos'][0]]
    # LUMO eV = data['moenergies'][0][data['homos'][0] + 1]